# Search ATSIFIO.dll (ATSIFIO64.dll)
function finddll()
    if haskey(ENV, "ANDOR_ATSIFIO_DLL")
        return ENV["ANDOR_ATSIFIO_DLL"]
    end
    if !Sys.iswindows()
        return ""
    end

    dllpath = ""

    AndorSIFReaderSDK = ["Andor SIF Reader SDK"]
    AndorSOLIS = ["Andor SOLIS", "SIF Reader", "SIFReaderSDK"]

    ProgramFiles = [ENV["PROGRAMFILES"], ENV["PROGRAMFILES(x86)"]]

    if Sys.WORD_SIZE == 64
        parentdir, dllname = "x64", "ATSIFIO64.dll"
    else
        parentdir, dllname = "win32", "ATSIFIO.dll"
    end
    for progfiles in ProgramFiles, andor in [AndorSIFReaderSDK, AndorSOLIS]
        path = joinpath(progfiles, andor..., parentdir, dllname)
        if isfile(path)
            dllpath = path
            break
        end
    end
    dllpath
end
