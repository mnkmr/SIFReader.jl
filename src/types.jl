# A sub-image in a frame
struct SifSubimage
    buffer::Array{Float32,1}
    geometry::Tuple{Int,Int,Int,Int}
    size::Tuple{Int,Int}
    hbin::Int
    vbin::Int

    function SifSubimage(buffer, left, bottom, right, top, hbin, vbin)
        geometry = (left, bottom, right, top)
        height = (right - left + 1)÷hbin
        width = (top - bottom + 1)÷vbin
        size = (height, width)
        new(buffer, geometry, size, hbin, vbin)
    end
end


# A data source in a SifData
struct SifDatasource
    # Name of datasource
    name::Symbol
    # Vector of frames in the data source
    frames::Array{Array{SifSubimage,1},1}
    # Dictionary of properties
    # Usually, it has the properties listed in SifDatasourceProperties in
    # src/properties.jl
    property::Dict{Symbol,Any}
end


# Empty data source
function SifDatasource(name)
    SifDatasource(name, Array{Array{SifSubimage,1},1}[], Dict{Symbol,Any}())
end


# A entire sif format file data
struct SifData
    # Datasources
    Signal::SifDatasource
    Reference::SifDatasource
    Background::SifDatasource
    Live::SifDatasource
    Source::SifDatasource
end

include("print.jl")
