function Base.show(io::IO, ::MIME"text/plain", z::SifSubimage)
    h, w = z.size
    l, b, r, t = z.geometry
    print("SIFReader.SifSubimage: $(h)x$(w) ($(l), $(b), $(r), $(t))")
end


# Pretty printing for SifDatasource
function Base.show(io::IO, ::MIME"text/plain", z::SifDatasource)
    print("SIFReader.SifDatasource:")
    print_datasource(z)
end


# Pretty printing for SifData
function Base.show(io::IO, ::MIME"text/plain", z::SifData)
    print("SIFReader.SifData:")
    print_datasource(z.Signal)
    print_datasource(z.Reference)
    print_datasource(z.Background)
    print_datasource(z.Live)
    print_datasource(z.Source)
end


function print_datasource(datasource)
    if isempty(datasource.frames)
        return
    end
    name = @sprintf("%10s", datasource.name)
    date = get(datasource.property, :FormattedTime, "")
    numframe = get(datasource.property, :NumberImages, 0)
    numsubimg = get(datasource.property, :NumberSubImages, 0)
    print("\n  $(name): [$(date)] $(numframe) frames, $(numsubimg) sub-images")
end
