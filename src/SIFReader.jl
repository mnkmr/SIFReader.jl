module SIFReader

using Printf

export readsif, readsifimg, hassource, countframe, countsubimg

include("sdk.jl"); using .SDK
include("types.jl")
include("errors.jl")

# Read a SIF format file to return a SifData typed value
function readsif(filename)
    sifopen(filename) do
        sig = datasource_get(Signal)
        ref = datasource_get(Reference)
        bg = datasource_get(Background)
        live = datasource_get(Live)
        src = datasource_get(Source)
        sifdata = SifData(sig, ref, bg, live, src)
    end
end


# Read a SIF format file to return an image array
function readsifimg(sifdata::SifData, frameidx::Integer, subimgidx::Integer,
                    source, T::Type=Float64)::Array{T,2}
    ds = datasource_get(sifdata, source)
    if isempty(ds.frames)
        error_source_not_exist(source)
    end
    frame = ds.frames[frameidx]
    subimg = frame[subimgidx]::SifSubimage
    buffer_toArray(subimg.buffer, subimg.size, T)
end

function readsifimg(sifdata::SifData, frameidx::Integer=1,
                    subimgidx::Integer=1; source=Signal)
    readsifimg(sifdata, frameidx, subimgidx, source)
end

function readsifimg(filename, frameidx::Integer, subimgidx::Integer,
                    source, T::Type=Float64)
    sifopen(filename) do
        if !ispresent(source)
            error_source_not_exist(source)
        end
        frame = frame_get(source, frameidx)
        subimg = frame[subimgidx]::SifSubimage
        buffer_toArray(subimg.buffer, subimg.size, T)
    end
end

function readsifimg(filename, frameidx::Integer=1, subimgidx::Integer=1;
                    source=Signal)
    readsifimg(filename, frameidx, subimgidx, source)
end


# Check if any data exists in a data source
function hassource(sifdata::SifData, source)
    ds = datasource_get(sifdata, source)
    !isempty(ds.frames)
end

function hassource(filename, source)
    sifopen(filename) do
        ispresent(source)
    end
end


# Return the number of frames
function countframe(sifdata::SifData; source=Signal)
    ds = datasource_get(sifdata, source)
    if isempty(ds.frames)
        return 0
    end
    length(ds.frames)
end

function countframe(filename; source=Signal)
    sifopen(filename) do
        if !ispresent(source)
            return 0
        end
        frame_num(source)
    end
end


# Return the number of sub-images
function countsubimg(sifdata::SifData; source=Signal)
    ds = datasource_get(sifdata, source)
    if isempty(ds.frames)
        return 0
    end
    length(ds.frames[1])
end

function countsubimg(filename; source=Signal)
    sifopen(filename) do
        if !ispresent(source)
            return 0
        end
        subimage_num(source)
    end
end


# Check if any file is loaded on memory
function isloaded()
    isloaded = Array{Cint,1}(undef, 1)
    rc = SDK.IsLoaded(isloaded)
    returncode_check(rc)
    isloaded[1] == 1
end


# Open ATSIFIO dll and a SIF format file for reading
# readmode should be a number in ATSIF_ReadMode enumeration
function sifopen(filename, readmode=ReadAll)
    if !isfile(filename)
        error_file_not_found(filename)
    end
    if isloaded()
        error_already_loaded()
    end

    rc = SDK.SetFileAccessMode(readmode)
    returncode_check(rc)

    rc = SDK.ReadFromFile(filename)
    returncode_check(rc)
    nothing
end

function sifopen(f::Function, filename, readmode=ReadAll)
    sifopen(filename, readmode)
    try
        f()
    finally
        sifclose()
    end
end


# Close both ATSIFIO dll and a SIF format file opened
function sifclose()
    rc = SDK.CloseFile()
    if rc != SDK.ATSIF_SUCCESS
        @error "sifclose() finishes abnormaly: return code $(rc)"
    end
    nothing
end


# Check if any data present in a kind of data sources
# source should be a number in ATSIF_DataSource enumeration
function ispresent(source)
    ispresent = Array{Cint,1}(undef, 1)
    rc = SDK.IsDataSourcePresent(source, ispresent)
    returncode_check(rc)
    ispresent[1] == 1
end


# Return a version of a structure element (?)
# A structure element is specified by `element` which should be a number in
# ATSIF_StructureElement enumeration
function version(element)
    hi = Array{Cuint,1}(undef, 1)
    lo = Array{Cuint,1}(undef, 1)
    rc = SDK.GetStructureVersion(element, hi, lo)
    returncode_check(rc)
    map(x -> Int(x[1]), (hi, lo))
end


const PROPERTYVALUELEN = 128

# Return a property as a string
# See Properties in properties.jl for possible `name`s
function property_value(source, name)
    val = Array{Cchar,1}(undef, PROPERTYVALUELEN)
    rc = SDK.GetPropertyValue(source, name, val)
    returncode_check(rc)
    val[end] = 0
    unsafe_string(pointer(val))
end


# Return a integer to specify a type of a property
# See Properties in properties.jl for possible `name`s
# The correspondence of the integer and a type is described in
# ATSIF_PropertyType enumeration in properties.jl
function property_type(source, name)
    typ = Array{Cuint,1}(undef, 1)
    rc = SDK.GetPropertyType(source, name, typ)
    returncode_check(rc)
    Int(typ[1])
end


function property_parse(t, value, name)
    if t == SDK.ATSIF_AT_8
        parse(Cchar, value)
    elseif t == SDK.ATSIF_AT_U8
        parse(Cuchar, value)
    elseif t == SDK.ATSIF_AT_32
        parse(Cint, value)
    elseif t == SDK.ATSIF_AT_U32
        parse(Cuint, value)
    elseif t == SDK.ATSIF_AT_64
        parse(Clong, value)
    elseif t == SDK.ATSIF_AT_U64
        parse(Culong, value)
    elseif t == SDK.ATSIF_Float
        parse(Cfloat, value)
    elseif t == SDK.ATSIF_Double
        parse(Cdouble, value)
    elseif t == SDK.ATSIF_String
        String(value)
    else
        # should not reach here
        error_unknown_property_type(name, t)
    end
end


# Return a property
# source should be a number in ATSIF_DataSource enumeration
function property_get(source, name)
    value = property_value(source, name)
    t = property_type(source, name)
    property_parse(t, value, name)
end

function property_get(source, dict::Dict{Symbol,String})
    plist = map(values(dict)) do name
        (Symbol(name), property_get(source, name))
    end
    Dict(plist)
end


# Return the number of sub-images in a frames
# source should be a number in ATSIF_DataSource enumeration
function subimage_num(source)
    num = zeros(Cuint, 1)
    rc = SDK.GetNumberSubImages(source, num)
    returncode_check(rc, allow=[SDK.ATSIF_DATA_NOT_PRESENT])
    Int(num[1])
end


# Return the range and bininng information of a image
# source should be a number in ATSIF_DataSource enumeration
function subimage_info(source, i)
    left = zeros(Cuint, 1)
    bottom = zeros(Cuint, 1)
    right = zeros(Cuint, 1)
    top = zeros(Cuint, 1)
    hbin = zeros(Cuint, 1)
    vbin = zeros(Cuint, 1)
    rc = SDK.GetSubImageInfo(source, i-1, left, bottom, right, top, hbin, vbin)
    returncode_check(rc, allow=[SDK.ATSIF_DATA_NOT_PRESENT])
    map(x -> Int(x[1]), (left, bottom, right, top, hbin, vbin))
end


# Return height and width of a sub-image
function subimage_size(left, bottom, right, top, hbin, vbin)
    if left == 0 || bottom == 0 || right == 0 || top == 0 || hbin == 0 || vbin == 0
        return 0, 0
    end
    height = (top - bottom + 1)÷vbin
    width = (right - left + 1)÷hbin
    height, width
end

function subimage_size(source, i)
    left, bottom, right, top, hbin, vbin = subimage_info(source, i)
    subimage_size(left, bottom, right, top, hbin, vbin)
end

function subimage_size(si::SifSubimage)
    subimage_size(si.info...)
end


# Return the total pixel number of a sub-image
# source should be a number in ATSIF_DataSource enumeration
function subimage_buffersize(source, i)
    height, width = subimage_size(source, i)
    height*width
end


# Return the total pixel number of all sub-images in a frame
# source should be a number in ATSIF_DataSource enumeration
function frame_buffersize(source)
    size = zeros(Cuint, 1)
    rc = SDK.GetFrameSize(source, size)
    returncode_check(rc, allow=[SDK.ATSIF_DATA_NOT_PRESENT])
    Int(size[1])
end


# Return the number of frames in a data source
# source should be a number in ATSIF_DataSource enumeration
function frame_num(source)
    num = zeros(Cuint, 1)
    rc = SDK.GetNumberFrames(source, num)
    returncode_check(rc, allow=[SDK.ATSIF_DATA_NOT_PRESENT])
    Int(num[1])
end


# Return a frame in a data source
# source should be a number in ATSIF_DataSource enumeration
function frame_get(source, i)::Array{SifSubimage,1}
    fbufsize = frame_buffersize(source)
    fbuf = Array{Cfloat,1}(undef, fbufsize)
    rc = SDK.GetFrame(source, i-1, fbuf, fbufsize)
    returncode_check(rc)

    sinum = subimage_num(source)
    sibufsizes = map(j -> subimage_buffersize(source, j), 1:sinum)
    siinfo = map(j -> subimage_info(source, j), 1:sinum)
    map(1:sinum) do j
        offset = sum(sibufsizes[1:(j-1)])
        chunksize = sibufsizes[j]
        sibuf = buffer_cut(fbuf, offset, chunksize)
        SifSubimage(sibuf, siinfo[j]...)
    end
end


# Return all frames in a data source
# source should be a number in ATSIF_DataSource enumeration
function frame_all(source)::Array{Array{SifSubimage,1},1}
    fnum = frame_num(source)
    fbufsize = frame_buffersize(source)
    totalsize = fbufsize*fnum
    totalbuffer = Array{Cfloat,1}(undef, totalsize)
    rc = SDK.GetAllFrames(source, totalbuffer, totalsize)
    returncode_check(rc)

    sinum = subimage_num(source)
    sibufsizes = map(i -> subimage_buffersize(source, i), 1:sinum)
    siinfo = map(i -> subimage_info(source, i), 1:sinum)
    map(1:fnum) do i
        map(1:sinum) do j
            foffset = fbufsize*(i-1)
            sioffset = sum(sibufsizes[1:(j-1)])
            offset = foffset + sioffset
            chunksize = sibufsizes[j]
            sibuf = buffer_cut(totalbuffer, offset, chunksize)
            SifSubimage(sibuf, siinfo[j]...)
        end
    end
end


function datasource_get(source)
    name = datasource_name(source)
    if !ispresent(source)
        return SifDatasource(name)
    end
    frames = frame_all(source)
    prop = property_get(source, SDK.SifDatasourceProperties)
    SifDatasource(name, frames, prop)
end

function datasource_get(sifdata::SifData, source)
    name = datasource_name(source)
    getproperty(sifdata, name)
end


function datasource_name(source)
    if source == Signal
        :Signal
    elseif source == Reference
        :Reference
    elseif source == Background
        :Background
    elseif source == Live
        :Live
    elseif source == Source
        :Source
    else
        error_unknown_source(source)
    end
end


function buffer_cut(buffer, offset, chunksize)
    i = offset + 1
    j = offset + chunksize
    buffer[i:j]
end


function buffer_toArray(buffer, size, T)
    height, width = size
    if eltype(buffer) == T
        return permutedims(reshape(buffer, height, width))
    end
    convert.(T, permutedims(reshape(buffer, height, width)))
end


function returncode_check(rc; allow=[])
    if rc == SDK.ATSIF_SUCCESS
        return nothing
    end
    if rc in allow
        return nothing
    end
    error_in_calling_dll_function(rc)
end


end # module
