module SDK

export ReadAll, ReadFromFile
export File, Insta, Calib, Andor
export Signal, Reference, Background, Live, Source

using Libdl, Dates

include("finddll.jl")
include("constants.jl")
include("properties.jl")

const DLL = Ref{Ptr{Cvoid}}(0)
function __init__()
    dllpath = finddll()
    if !isfile(dllpath)
        dllname = Sys.WORD_SIZE == 64 ? "ATSIFIO64.dll" : "ATSIFIO.dll"
        @error "SIFReader: $(dllname) not found"
    end
    DLL[] = dlopen(dllpath)
    nothing
end


function IsLoaded(isloaded::Array{Cint,1})
    F = dlsym(DLL[], :ATSIF_IsLoaded)
    ccall(F, UInt32, (Ref{Cint},), isloaded)
end


function SetFileAccessMode(readmode::Integer)
    F = dlsym(DLL[], :ATSIF_SetFileAccessMode)
    ccall(F, UInt32, (Cuint,), readmode)
end


function ReadFromFile(filename::AbstractString)
    F = dlsym(DLL[], :ATSIF_ReadFromFile)
    ccall(F, UInt32, (Cstring,), filename)
end


function CloseFile()
    F = dlsym(DLL[], :ATSIF_CloseFile)
    ccall(F, UInt32, ())
end


function IsDataSourcePresent(source::Integer, ispresent::Array{Cint,1})
    F = dlsym(DLL[], :ATSIF_IsDataSourcePresent)
    ccall(F, UInt32, (Cuint, Ref{Cint}), source, ispresent)
end


function GetStructureVersion(element::Integer, hi::Array{Cuint,1},
                             lo::Array{Cuint,1})
    F = dlsym(DLL[], :ATSIF_GetStructureVersion)
    ccall(F, UInt32, (Cuint, Ref{Cuint}, Ref{Cuint}), element, hi, lo)
end


function GetPropertyValue(source::Integer, name::String, val::Array{Cchar,1})
    len = length(val)
    F = dlsym(DLL[], :ATSIF_GetPropertyValue)
    ccall(F, UInt32, (Cuint, Cstring, Ref{Cchar}, Cuint), source, name, val, len)
end


function GetPropertyType(source::Integer, name::String, typ::Array{Cuint,1})
    F = dlsym(DLL[], :ATSIF_GetPropertyType)
    ccall(F, UInt32, (Cuint, Cstring, Ref{Cuint}), source, name, typ)
end


function GetNumberSubImages(source::Integer, num::Array{Cuint,1})
    F = dlsym(DLL[], :ATSIF_GetNumberSubImages)
    ccall(F, UInt32, (Cuint, Ref{Cuint}), source, num)
end


function GetSubImageInfo(source::Integer, i::Integer,
                         left::Array{Cuint,1}, bottom::Array{Cuint,1},
                         right::Array{Cuint,1}, top::Array{Cuint,1},
                         hbin::Array{Cuint,1}, vbin::Array{Cuint,1})
    F = dlsym(DLL[], :ATSIF_GetSubImageInfo)
    ccall(F, UInt32, (Cuint, Cuint, Ref{Cuint}, Ref{Cuint}, Ref{Cuint},
                      Ref{Cuint}, Ref{Cuint}, Ref{Cuint}),
          source, i, left, bottom, right, top, hbin, vbin)
end


function GetFrameSize(source::Integer, size::Array{Cuint,1})
    F = dlsym(DLL[], :ATSIF_GetFrameSize)
    ccall(F, UInt32, (Cuint, Ref{Cuint}), source, size)
end


function GetNumberFrames(source::Integer, num::Array{Cuint,1})
    F = dlsym(DLL[], :ATSIF_GetNumberFrames)
    ccall(F, UInt32, (Cuint, Ref{Cuint}), source, num)
end


function GetFrame(source::Integer, i::Integer, fbuf::Array{Cfloat,1},
                  fbufsize::Integer)
    F = dlsym(DLL[], :ATSIF_GetFrame)
    ccall(F, UInt32, (Cuint, Cuint, Ref{Cfloat}, Cuint),
          source, i, fbuf, fbufsize)
end


function GetAllFrames(source::Integer, totalbuffer::Array{Cfloat,1},
                      totalsize::Integer)
    F = dlsym(DLL[], :ATSIF_GetAllFrames)
    rc = ccall(F, UInt32, (Cuint, Ref{Cfloat}, Cuint),
               source, totalbuffer, totalsize)
end


end
