# ATSIF_ReadMode enumeration
const ReadAll        = 0x40000000
const ReadHeaderOnly = 0x40000001


# ATSIF_StructureElement enumeration
const File  = 0x40000000
const Insta = 0x40000001
const Calib = 0x40000002
const Andor = 0x40000003


# ATSIF_DataSource enumeration
const Signal     = 0x40000000
const Reference  = 0x40000001
const Background = 0x40000002
const Live       = 0x40000003
const Source     = 0x40000004


# ATSIF_PropertyType enumeration
const ATSIF_AT_8   = 0x40000000
const ATSIF_AT_U8  = 0x00000001
const ATSIF_AT_32  = 0x40000002
const ATSIF_AT_U32 = 0x40000003
const ATSIF_AT_64  = 0x40000004
const ATSIF_AT_U64 = 0x40000005
const ATSIF_Float  = 0x40000006
const ATSIF_Double = 0x40000007
const ATSIF_String = 0x40000008


# Return codes
# Normal exit
const ATSIF_SUCCESS = 22002

# Internal error
const ATSIF_SIF_FORMAT_ERROR = 22003
const ATSIF_NO_SIF_LOADED = 22004
const ATSIF_FILE_NOT_FOUND = 22005
const ATSIF_FILE_ACCESS_ERROR = 22006
const ATSIF_DATA_NOT_PRESENT = 22007

# Invalid argument (?)
const ATSIF_P1INVALID = 22101
const ATSIF_P2INVALID = 22102
const ATSIF_P3INVALID = 22103
const ATSIF_P4INVALID = 22104
const ATSIF_P5INVALID = 22105
const ATSIF_P6INVALID = 22106
const ATSIF_P7INVALID = 22107
const ATSIF_P8INVALID = 22108
