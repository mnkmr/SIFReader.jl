using Test, SIFReader, Libdl, Dates, DelimitedFiles

const TESTFILE = joinpath(dirname(@__FILE__), "test.sif")
const TSVFILE = joinpath(dirname(@__FILE__), "test.asc")

const TESTBUFFER = zeros(Float32, 100)
const TESTIMAGE = Float64.(transpose(reshape(copy(TESTBUFFER), 10, 10)))

# return a SifData instance for test
function testSifData()
    buf = copy(TESTBUFFER)
    subimg = SIFReader.SifSubimage(buf, 1, 1, 10, 10, 1, 1)
    sig = SIFReader.SifDatasource(:Signal, [[subimg]], Dict{Symbol,Any}())
    ref = SIFReader.SifDatasource(:Reference)
    bg = SIFReader.SifDatasource(:Background)
    live = SIFReader.SifDatasource(:Live)
    src = SIFReader.SifDatasource(:Source)
    SIFReader.SifData(sig, ref, bg, live, src)
end

try
    @testset "SIFReader: internal functions" begin
        @testset "isloaded" begin
            @test SIFReader.isloaded() == false
            SIFReader.sifopen(TESTFILE)
            @test SIFReader.isloaded() == true
            SIFReader.sifclose()
            @test SIFReader.isloaded() == false
        end


        @testset "ispresent" begin
            SIFReader.sifopen(TESTFILE)
            @test SIFReader.ispresent(SIFReader.Signal) == true
            @test SIFReader.ispresent(SIFReader.Reference) == false
            @test SIFReader.ispresent(SIFReader.Background) == false
            @test SIFReader.ispresent(SIFReader.Live) == false
            @test SIFReader.ispresent(SIFReader.Source) == false
            SIFReader.sifclose()
        end

        @testset "subimage_num" begin
            SIFReader.sifopen(TESTFILE)
            @test SIFReader.subimage_num(SIFReader.Signal) == 1
            @test SIFReader.subimage_num(SIFReader.Reference) == 0
            @test SIFReader.subimage_num(SIFReader.Background) == 0
            @test SIFReader.subimage_num(SIFReader.Live) == 0
            @test SIFReader.subimage_num(SIFReader.Source) == 0
            SIFReader.sifclose()
        end

        @testset "subimage_info" begin
            SIFReader.sifopen(TESTFILE)
            @test SIFReader.subimage_info(SIFReader.Signal, 1) == (1, 1, 1004, 1002, 1, 1)
            @test SIFReader.subimage_info(SIFReader.Reference, 1) == (0, 0, 0, 0, 0, 0)
            @test SIFReader.subimage_info(SIFReader.Background, 1) == (0, 0, 0, 0, 0, 0)
            @test SIFReader.subimage_info(SIFReader.Live, 1) == (0, 0, 0, 0, 0, 0)
            @test SIFReader.subimage_info(SIFReader.Source, 1) == (0, 0, 0, 0, 0, 0)
            SIFReader.sifclose()
        end

        @testset "subimage_size" begin
            SIFReader.sifopen(TESTFILE)
            @test SIFReader.subimage_size(SIFReader.Signal, 1) == (1002, 1004)
            @test SIFReader.subimage_size(SIFReader.Reference, 1) == (0, 0)
            @test SIFReader.subimage_size(SIFReader.Background, 1) == (0, 0)
            @test SIFReader.subimage_size(SIFReader.Live, 1) == (0, 0)
            @test SIFReader.subimage_size(SIFReader.Source, 1) == (0, 0)
            SIFReader.sifclose()
        end

        @testset "subimage_buffersize" begin
            SIFReader.sifopen(TESTFILE)
            @test SIFReader.subimage_buffersize(SIFReader.Signal, 1) == 1006008
            @test SIFReader.subimage_buffersize(SIFReader.Reference, 1) == 0
            @test SIFReader.subimage_buffersize(SIFReader.Background, 1) == 0
            @test SIFReader.subimage_buffersize(SIFReader.Live, 1) == 0
            @test SIFReader.subimage_buffersize(SIFReader.Source, 1) == 0
            SIFReader.sifclose()
        end

        @testset "frame_num" begin
            SIFReader.sifopen(TESTFILE)
            @test SIFReader.frame_num(SIFReader.Signal) == 1
            @test SIFReader.frame_num(SIFReader.Reference) == 0
            @test SIFReader.frame_num(SIFReader.Background) == 0
            @test SIFReader.frame_num(SIFReader.Live) == 0
            @test SIFReader.frame_num(SIFReader.Source) == 0
            SIFReader.sifclose()
        end

        @testset "frame_buffersize" begin
            SIFReader.sifopen(TESTFILE)
            @test SIFReader.frame_buffersize(SIFReader.Signal) == 1006008
            @test SIFReader.frame_buffersize(SIFReader.Reference) == 0
            @test SIFReader.frame_buffersize(SIFReader.Background) == 0
            @test SIFReader.frame_buffersize(SIFReader.Live) == 0
            @test SIFReader.frame_buffersize(SIFReader.Source) == 0
            SIFReader.sifclose()
        end
    end

    @testset "SIFReader: exported functions" begin
        @testset "readsif" begin
            sifdata = readsif(TESTFILE)
            @test typeof(sifdata) == SIFReader.SifData

            # check if the file is safely closed
            @test SIFReader.isloaded() == false
        end

        sifdata = testSifData()

        @testset "readsifimg" begin
            @test (readsifimg(TESTFILE) ==
                   transpose(readdlm(TSVFILE, Float64)[:, 2:end]))
            @test (readsifimg(TESTFILE, 1) ==
                   transpose(readdlm(TSVFILE, Float64)[:, 2:end]))
            @test (readsifimg(TESTFILE, 1, 1) ==
                   transpose(readdlm(TSVFILE, Float64)[:, 2:end]))
            @test (readsifimg(TESTFILE, source=SIFReader.Signal) ==
                   transpose(readdlm(TSVFILE, Float64)[:, 2:end]))
            @test_throws ErrorException readsifimg(TESTFILE, source=SIFReader.Reference) == transpose(readdlm(TSVFILE, Float64)[:, 2:end])

            # check if the file is safely closed
            @test SIFReader.isloaded() == false

            @test readsifimg(sifdata) == TESTIMAGE
            @test readsifimg(sifdata, 1) == TESTIMAGE
            @test readsifimg(sifdata, 1, 1) == TESTIMAGE
            @test readsifimg(sifdata, source=SIFReader.Signal) == TESTIMAGE

            # The use of type parameter of readsifimg()
            ## This is the default behavior: convert to Array{Float64,2}
            @test eltype(readsifimg(sifdata, 1, 1, SIFReader.Signal, Float64)) == Float64
            ## Return an image in the native float accuracy: this is faster than the above
            @test eltype(readsifimg(sifdata, 1, 1, SIFReader.Signal, Float32)) == Float32
            @test_throws ErrorException readsifimg(sifdata, source=SIFReader.Reference) == TESTIMAGE
        end

        @testset "hassource" begin
            @test hassource(TESTFILE, SIFReader.Signal) == true
            @test hassource(TESTFILE, SIFReader.Reference) == false

            # check if the file is safely closed
            @test SIFReader.isloaded() == false

            @test hassource(sifdata, SIFReader.Signal) == true
            @test hassource(sifdata, SIFReader.Reference) == false
        end

        @testset "countframe" begin
            @test countframe(TESTFILE) == 1
            @test countframe(TESTFILE, source=SIFReader.Signal) == 1
            @test countframe(TESTFILE, source=SIFReader.Reference) == 0

            # check if the file is safely closed
            @test SIFReader.isloaded() == false

            @test countframe(sifdata) == 1
            @test countframe(sifdata, source=SIFReader.Signal) == 1
            @test countframe(sifdata, source=SIFReader.Reference) == 0
        end

        @testset "countsubimg" begin
            @test countsubimg(TESTFILE) == 1
            @test countsubimg(TESTFILE, source=SIFReader.Signal) == 1
            @test countsubimg(TESTFILE, source=SIFReader.Reference) == 0

            # check if the file is safely closed
            @test SIFReader.isloaded() == false

            @test countsubimg(sifdata) == 1
            @test countsubimg(sifdata, source=SIFReader.Signal) == 1
            @test countsubimg(sifdata, source=SIFReader.Reference) == 0
        end
    end
finally
    SIFReader.sifclose()
end
