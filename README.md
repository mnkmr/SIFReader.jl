# Reading Andor SIF format file through the official API

This package provides the ability to read data in Andor's SIF format file from [Julia language](https://julialang.org/).


## Installation

### 1. Install Andor SIF Reader SDK

This package makes use of the official Andor SIF Reader API (and thus, only Windows OS is supported). It is required to install [Andor SOLIS](https://andor.oxinst.com/products/solis-software/) or Andor SIF Reader SDK (provided in [the download page](http://www.andor.com/downloads) in Andor website) before using.


### 2. Install SIFReader.jl

Open Julia REPL and press `]` key to start the Pkg REPL-mode, then type:

```
(v1.0) pkg> add https://gitlab.com/mnkmr/SIFReader.jl
```


## Usage

First of all, load SIFReader library.

```julia
using SIFReader
```

If you got "ATSIFIO.dll not found" error, see [the note](#if-you-got-an-error-in-using-sifreader) on it.


### Retrieve an image as an Array

```julia
img = readsifimg("path/to/*.sif")
```


### Check the number of frames in a data source

```julia
nframe = countframe("path/to/*.sif")
```


### Check the number of sub-images in a data source

```julia
nsubimg = countsubimg("path/to/*.sif")
```


### Retrieve an image from *n*-th frame

```julia
# retrieve a image from third frame
n = 3
img = readsifimg("path/to/*.sif", n)
```

Assign a number to the second parameter to specify a frame. It is 1 if omitted.
The *n* should be inside the range `1 ≤ n ≤ countframe("path/to/*.sif")`.


### Retrieve the *m*-th sub-image in the *n*-th frame

```julia
# retrieve the second sub-image from the third frame
n = 3
m = 2
img = readsifimg("path/to/*.sif", n, m)
```

Assign a number to the third parameter to specify a frame. It is 1 if omitted.
The *m* should be inside the range `1 ≤ n ≤ countsubimg("path/to/*.sif")`.


### Get data from another data source

The above examples obtained data from "Signal" channel.
Those functions use "Signal" as the default data source; if you want to retrieve data from another source, assign the desired one of them.

```julia
const SIGNAL = SIFReader.Signal
const REFERENCE = SIFReader.Reference
const BACKGROUND = SIFReader.Background
const LIVE = SIFReader.Live
const SOURCE = SIFReader.Source

nframe = countframe("path/to/*.sif", source=SIGNAL)
nsubimg = countsubimg("path/to/*.sif", source=SIGNAL)
img = readsifimg("path/to/*.sif", nframe, nsubimg, source=SIGNAL)

nframe = countframe("path/to/*.sif", source=REFERENCE)
nsubimg = countsubimg("path/to/*.sif", source=REFERENCE)
img = readsifimg("path/to/*.sif", nframe, nsubimg, source=REFERENCE)

nframe = countframe("path/to/*.sif", source=BACKGROUND)
nsubimg = countsubimg("path/to/*.sif", source=BACKGROUND)
img = readsifimg("path/to/*.sif", nframe, nsubimg, source=BACKGROUND)

nframe = countframe("path/to/*.sif", source=LIVE)
nsubimg = countsubimg("path/to/*.sif", source=LIVE)
img = readsifimg("path/to/*.sif", nframe, nsubimg, source=LIVE)

nframe = countframe("path/to/*.sif", source=SOURCE)
nsubimg = countsubimg("path/to/*.sif", source=SOURCE)
img = readsifimg("path/to/*.sif", nframe, nsubimg, source=SOURCE)
```


### Check if a data source exists or not

```julia
const SIGNAL = SIFReader.Signal
const REFERENCE = SIFReader.Reference
const BACKGROUND = SIFReader.Background
const LIVE = SIFReader.Live
const SOURCE = SIFReader.Source

if hassource("path/to/*.sif", SIGNAL)
    println("Data source Signal exists!")
end

if hassource("path/to/*.sif", REFERENCE)
    println("Data source Reference exists!")
end

if hassource("path/to/*.sif", BACKGROUND)
    println("Data source Background exists!")
end

if hassource("path/to/*.sif", LIVE)
    println("Data source Live exists!")
end

if hassource("path/to/*.sif", SOURCE)
    println("Data source Source exists!")
end
```


### Retrieve all data in a .sif file

```julia
sifdata = readsif("path/to/*.sif")
```

This `sifdata` is an instance of `SIFReader.SifData` type.
The structure of `SIFReader.SifData` is written in `src/types.jl`.


### Extract a portion of data from a SifData instance

```julia
sifdata = readsif("path/to/*.sif")

# check the existence of Signal source
const SIGNAL = SIFReader.Signal
if hassource(sifdata, SIGNAL)
    # check the number of frames
    nframe = countframe(sifdata)

    # check the number of sub-images
    nsubimg = countsubimg(sifdata)

    # retrieve an image
    img = readsifimg(sifdata)
end
```


## Note

### If you got an error in `using SIFReader`

It may be because SIFReader.jl cannot find the dll which provides API for reading SIF file.

 - If you are using 64bit Julia, SIFReader.jl needs `ATSIFIO64.dll`; it would be in `{Andor SIF Reader SDK installation path}\\x64\\ATSIFIO64.dll` or `{Andor SOLIS installation path}\\SIF\ Reader\\SIFReaderSDK\\x64\\ATSIFIO64.dll`.

 - If you are using 32bit Julia, SIFReader.jl needs `ATSIFIO.dll`; it would be in `{Andor SIF Reader SDK installation path}\\win32\\ATSIFIO.dll` or `{Andor SOLIS installation path}\\SIF\ Reader\\SIFReaderSDK\\win32\\ATSIFIO.dll`.

This package searches the dll under `%PROGRAMFILES%` and under `%PROGRAMFILES(x86)%` automatically; if you installed in another location, specify the full path to `ANDOR_ATSIFIO_DLL` environmental variable.

```julia
ENV["ANDOR_ATSIFIO_DLL"] = "D:\\Foo\\Bar\\Baz\\Andor\ SIF\ Reader\ SDK\\x64\\ATSIFIO64.dll"
```
